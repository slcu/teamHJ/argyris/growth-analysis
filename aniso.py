import numpy as np
from scipy.spatial import ConvexHull, KDTree
from scipy.linalg import lstsq
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import common.lin as lin
import common.seg as seg

def centre_points(ps):
    return ps - np.mean(ps, axis=0)

def merge_points(cell_regions, cids):
    ps = list()
    for cid in cids:
        if cid in cell_regions:
            ps.append(cell_regions[cid])

    if ps:
        return np.vstack(tuple(ps))
    else:
        return np.array([])

def fract_anisotropy(s):
    tr = np.mean(s)
    return (np.sqrt(1.5) * (np.sqrt(np.sum((s - tr)**2)) / np.sqrt(np.sum(s**2))))

def fract_anisotropy1(s):
    tr = np.mean(s)
    return (np.sqrt(1.5) * (np.sqrt(np.sum((s - tr)**2))))

def eival_anisotropy(s):
    return s[0] / s[1]

def hulls(regs, linss, t1, t2, i):
    ds = linss[(t1, t2)][i]

    if not ds:
        return 0.0, np.array([]), np.array([])

    ps1 = centre_points(regs[t1][i])
    ps2 = centre_points(merge_points(regs[t2], ds))

    h1 = ConvexHull(ps1)
    h2 = ConvexHull(ps2)

    vs1 = h1.points[h1.vertices]
    vs2 = h2.points[h2.vertices]

    return vs1, vs2

def ganiso_f(regs, linss, t1, t2, i, descr=fract_anisotropy):
    dt = t2 - t1

    linsL = lin.filterLinssBetween(linss, (t1, t2))

    ds = lin.tSearchN(linsL, i, len(linsL), 0)

    if not ds:
        return None, np.array([]), np.array([])

    ps1 = centre_points(regs[t1][i])
    ps2_ = merge_points(regs[t2], ds)
    if not list(ps2_):
        return None, np.array([]), np.array([])
    ps2 = centre_points(ps2_)


    h1 = ConvexHull(ps1)
    h2 = ConvexHull(ps2)

    vs1 = h1.points[h1.vertices]
    vs2 = h2.points[h2.vertices]

    vs2_q = KDTree(vs2)
    _, ids = vs2_q.query(vs1)
    vs2_ = vs2[ids, :]

    A = lstsq(vs1, vs2_)

    _, s, _ = np.linalg.svd(A[0])

    return (descr(s) / dt), vs1, vs2

def get_eivals(D):
    cv = np.cov(D)
    eivals, eivecs = np.linalg.eigh(cv)
    key = np.argsort(eivals)[::-1][:]
    eivals, eivecs = eivals[key], eivecs[:, key]

    return eivals, eivecs

def sample_points(D):
    return D[0:len(D):2, :]

def ganiso_f_pca(regs, linss, t1, t2, i):
    dt = t2 - t1

    ds = linss[(t1, t2)][i]

    if not ds:
        return 0.0, np.array([]), np.array([])

    ps1 = sample_points(centre_points(regs[t1][i]))
    ps2 = sample_points(centre_points(merge_points(regs[t2], ds)))

    eivals1, _ = get_eivals(ps1.T)
    eivals2, _ = get_eivals(ps2.T)
    
    a1 = eival_anisotropy(eivals1)
    a2 = eival_anisotropy(eivals2)

    return (((a2-a1) / a1) / dt), ps1, ps2

def plot_points3(vs1, vs2):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(vs1[:, 0], vs1[:, 1], vs1[:, 2], c='b')
    ax.scatter(vs2[:, 0], vs2[:, 1], vs2[:, 2], c='r')

    plt.show()

def plot_points2(vs1, vs2):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.scatter(vs1[:, 0], vs1[:, 1], c='b')
    ax.scatter(vs3[:, 0], vs3[:, 1], c='g')

    for i in range(len(vs1)):
        ax.annotate(str(i), (vs1[i, 0], vs1[i, 1]))

    for i in range(len(vs2)):
        ax.annotate(str(i), (vs2[i, 0], vs2[i, 1]))

    plt.show()

def ganisos_forward(regs, tss, linss, t1, t2, descr=fract_anisotropy):
    ans_ = []
    for c in tss[t1]:
        an, _, _ = ganiso_f(regs, linss, t1, t2, c.cid, descr=descr)
        if an:
            ans_.append((c.cid, an))

    return dict(ans_)

def ganisos_backward(regs, tss, linss, t1, t2):
    linsL = lin.filterLinssBetween(linss, (t1, t2))
    ans_ = []

    for c in tss[t1]:
        an, _, _ = ganiso_f(regs, linss, t1, t2, c.cid)
        ds = lin.tSearchN(linsL, c.cid, len(linsL), 0)
        for d in ds:
            ans_.append((d, an))

    return dict(ans_)

def ganisos_backward_data(gans, tss, linss, t1, t2):
    ans_ = []
    im = lin.invertMap(linss[(t1, t2)])
    for d in tss[t2].cells.keys():
        i = im.get(d, -1)
        if i > 0:
            an = gans[t1].get(i, 0.0)
            ans_.append((d, an))
        else:
            ans_.append((d, 0.0))

    return dict(ans_)

def plot_cells_aniso(ts, ans):
    ps = np.array([[c.pos.x, c.pos.y, c.pos.z] for c in ts])
    cs = [ans.get(c.cid, 0.0) for c in ts]

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    ax.scatter(ps[:, 0], ps[:, 1], ps[:, 2], c=cs, s=100, linewidths=0.5, 
              cmap=plt.cm.hot, alpha=0.9, edgecolors='black', vmin=0.1, vmax=0.8)

    plt.show()


def plot_pca(D):
    Dc = centre_points(D)
    eivals, eivecs = get_eivals(Dc.T)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    ax.scatter(Dc[:, 0], Dc[:, 1], Dc[:, 2], c='b')
    ax.plot([0, eivals[0]*eivecs[0, 0]],
            [0, eivals[0]*eivecs[0, 1]],
            [0, eivals[0]*eivecs[0, 2]],
            c='r')

    ax.plot([0, eivals[1]*eivecs[1, 0]],
            [0, eivals[1]*eivecs[1, 1]],
            [0, eivals[1]*eivecs[1, 2]],
            c='r')
    
    ax.plot([0, eivals[2]*eivecs[2, 0]],
            [0, eivals[2]*eivecs[2, 1]],
            [0, eivals[2]*eivecs[2, 2]],
            c='r')

    plt.show()

    return eivecs, eivals

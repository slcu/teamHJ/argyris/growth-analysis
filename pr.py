from typing import List, Dict, Union, Set
import numpy as np
import common.seg as seg
import common.lin as lin
import grates
import growth_control as gc


Num = Union[int, float]


def cluster_grs(grs_cs: Dict[int, float]):
    grs_state = grs_state = gc.get_grs_state()
    g, xsm, ysm, cls = gc.cluster_states(grs_state)

    cls_ = {cid: np.argmin(np.abs(xsm - gr))
            for cid, gr in grs_cs.items()}

    return cls_


def read_csv(fn: str) -> List[Dict[str, Num]]:
    def f_key(k, v):
        if k == "id":
            return int(v)
        else:
            return float(v)

    with open(fn) as fout:
        header = fout.readline().strip().split()

        cs = list()
        for ln in fout:
            elems = [el.strip() for el in ln.split()]
            cs.append(dict([(k, f_key(k, v)) for k, v in zip(header, elems)]))

    return cs


def write_csv(fn: str, elems: List[Dict[str, Num]]):
    with open(fn, "w") as fout:
        header = " ".join([k for k, _ in elems[0].items()])
        lns = [header]
        for elem in elems:
            lns.append(" ".join([str(v) for _, v in elem.items()]))

        fout.write("\n".join(lns))


def add_k(kid: str,
          k_: str,
          d1: Dict[str, Num],
          d2: Dict[Num, Num]):
    id = d1[kid]
    d1[k_] = d2.get(id, 0)


def replace_val(kid: str,
                d1: Dict[str, Num],
                d2: Dict[Num, Num]):
    id = d1[kid]
    d1[kid] = d2[id]


def gets(d: Dict[str, Num], ks: List[str]) -> List[Num]:
    return [d[k] for k in ks]


def filter_ds(ds: List[Dict[str, Num]],
              k: str,
              fs: Set[Num]) -> List[Dict[str, Num]]:
    return [d for d in ds if d[k] in fs]


def to_np_arrays(d: List[Dict[str, Num]], xks: List[str], yk: str):
    X = np.array([gets(el, xks) for el in d])
    Y = np.array([gets(el, [yk])[0] for el in d])

    return X, Y


def go():
    gexprs = read_csv("data/states2.csv")
    tss, linss = lin.mkSeries1(d="../data/FM1/tv/",
                               dExprs="../data/geneExpression/",
                               linDataLoc="../data/FM1/tracking_data/",
                               ft=lambda t: t in {10, 40, 96, 120, 132})

    grss = grates.grates_avg_cons(tss, linss)

    for g in gexprs:
        add_k("id", "grate", g, grss[132])

    return to_np_arrays(gexprs, seg.geneNms, "grate")


def classify_cells():
    from sklearn.svm import LinearSVC

    gexprs = read_csv("data/states2.csv")
    tss, linss = lin.mkSeries1(d="../data/FM1/tv/",
                               dExprs="../data/geneExpression/",
                               linDataLoc="../data/FM1/tracking_data/",
                               ft=lambda t: t in {10, 40, 96, 120, 132})
    lin.filterL1_st(tss)
    cids_L1 = set([c.cid for c in tss[132]])
    gexprs = filter_ds(gexprs, "id", cids_L1)

    grss = grates.grates_avg_cons(tss, linss)
    grss = {cid: gr for cid, gr in grss[132].items() if cid in cids_L1}
    grs_cs = cluster_grs(grss)

    for g in gexprs:
        add_k("id", "grc", g, grs_cs)

    X, y = to_np_arrays(gexprs, seg.geneNms, "grc")
    clf = LinearSVC(random_state=0, tol=1e-5)
    clf.fit(X, y)
    grs_cs_pred = dict(list(zip(cids_L1, clf.predict(X))))

    for g in gexprs:
        add_k("id", "grc_pred", g, grs_cs_pred)

    return clf, gexprs


def predict_grs():
    from sklearn.svm import LinearSVR

    gexprs = read_csv("data/states2.csv")
    tss, linss = lin.mkSeries1(d="../data/FM1/tv/",
                               dExprs="../data/geneExpression/",
                               linDataLoc="../data/FM1/tracking_data/",
                               ft=lambda t: t in {10, 40, 96, 120, 132})
    # relabelM = {i: c.cid for i, c in enumerate(tss[132])}
    # for g in gexprs:
    #     replace_val("id", g, relabelM)
        
    lin.filterL1_st(tss)
    cids_L1 = set([c.cid for c in tss[132]])
    
    gexprs = filter_ds(gexprs, "id", cids_L1)

    grss = grates.grates_avg_cons(tss, linss)
    grss = {cid: gr for cid, gr in grss[132].items() if cid in cids_L1}

    for g in gexprs:
        add_k("id", "grate", g, grss)

    inputs = list(set(seg.geneNms) - set(['STM']))

    X, y = to_np_arrays(gexprs, inputs, "grate")
    regr = LinearSVR(random_state=0, max_iter=100000)
    regr.fit(X, y)
    grates_pred = dict(list(zip(cids_L1, regr.predict(X))))

    for g in gexprs:
        add_k("id", "grate_pred", g, grates_pred)

    return regr, gexprs, inputs


def grs_lfy_mutant(gexprs, regr):
    for g in gexprs:
        g["LFY"] = 0.0

    cids = [g["id"] for g in gexprs]
    X, y = to_np_arrays(gexprs, seg.geneNms, "grate")
    grates_pred = dict(list(zip(cids, regr.predict(X))))

    for g in gexprs:
        add_k("id", "grate_pred_lfy", g, grates_pred)

    return gexprs

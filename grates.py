import numpy as np
import common.lin as lin
import common.edict as edict

def fst(x): return x[0]

def snd(x): return x[1]

def avg2(v1, v2):
    return np.mean([v1, v2])

def calcVolumes(tss, linss, t1, t2):
    tsL = lin.filterTssBetween(tss, (t1, t2))
    linsL = lin.filterLinssBetween(linss, (t1, t2))
    volss = lin.mergeCellVolsRefN(tsL, linsL)

    cids = tsL[0].cells.keys()
    cids = [cid for cid in cids if not volss[-1][cid] == 0]

    volss1 = dict([(cid, [vols[cid] for vols in volss]) for cid in cids])

    return volss1

def grates_forward(tss, linss, t1, t2):
    volss1 = calcVolumes(tss, linss, t1, t2)

    grs = dict()
    for c, vols_c in volss1.items():
        grs[c] = ((np.log(vols_c[-1])-np.log(vols_c[0]))) / (t2-t1)
        
    return grs

def grates_backward(tss, linss, t1, t2):
    linsL = lin.filterLinssBetween(linss, (t1, t2))
    volss1 = calcVolumes(tss, linss, t1, t2)

    grs = dict()
    for c, vols_c in volss1.items():
        ds = lin.tSearchN(linsL, c, len(linsL), 0)
        for d in ds:
            grs[d] = ((np.log(vols_c[-1])-np.log(vols_c[0]))) / (t2-t1)
            
    return grs

def grates_forward_cons(tss, linss, succ_f=lin.succ):
    tpoints = sorted(tss.keys())
    grs_f = dict([(t, grates_forward(tss, linss, t, succ_f(t)))
                  for t in tpoints if succ_f(t)])

    return grs_f

def grates_backward_cons(tss, linss, prev_f=lin.prev):
    tpoints = sorted(tss.keys())
    grs_b = dict([(t, grates_backward(tss, linss, prev_f(t), t))
                  for t in tpoints if prev_f(t)])

    return grs_b

def grates_avg_cons(tss, linss, succ_f, prev_f):
    tpoints = sorted(tss.keys())
    grs_f = grates_forward_cons(tss, linss, succ_f)
    grs_b = grates_backward_cons(tss, linss, prev_f)
    
    grs_avg = edict.unionWith(grs_f, grs_b,
                              lambda m1, m2: edict.unionWith(m1, m2, avg2))

    return grs_avg

def write_grs_mnet(t, d):
     header = "type:float" 
     content = "\n".join([header] +
                         ["{t},{k}:{v}".format(t=t, k=k, v=v)
                          for k, v in d.items()])
     
     return content

def write_grs_csv(d):
    pass
